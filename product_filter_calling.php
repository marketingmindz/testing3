<?php
$path_to_root="..";
include_once($path_to_root . "/reporting/includes/db/report_db.inc");
include_once($path_to_root . "/includes/session.inc");
if(isset($_REQUEST['category']))
{
	$category = $_REQUEST['category'];
	$sheet_type = $_REQUEST['sheet_type'];
	$sql = "SELECT * FROM 0_finish_product WHERE category_id=$category ORDER BY finish_product_name ASC";
	$res = db_query($sql, "Not retrieved");

	if($sheet_type == '1')
	{
		echo '<td class="label">Choose Product : </td>';
		echo '<td><select autocomplete="off" class="combo" name="select_single_product" id="select_single_product" style="width:300px;">'; 
		echo "<option value=''>-- Select Product --</option>";
		while($row = db_fetch($res)) {
			$selected = "";
			echo "<option value='".$row['finish_pro_id']."' ".$selected.">".$row['finish_product_name']."</option>";
		}
		echo '</select></td>';
	}else if($sheet_type == '2')
	{
		$sql2 = "SELECT * FROM 0_finish_product WHERE category_id=$category ORDER BY finish_product_name ASC";
		$res2 = db_query($sql2, "Not retrieved");
		echo '<td class="label">Choose Product : </td>';
		echo '<td><select autocomplete="off" class="combo" name="select_multi_product1" id="select_multi_product1" style="width:300px;" _last="0"><option value="">-- Select Product 1 --</option>'; 
		while($row = db_fetch($res)) {
			$selected = "";
			echo "<option value='".$row['finish_pro_id']."' ".$selected.">".$row['finish_product_name']."</option>";
		}
		echo '</select></td>';

		echo '<td><select autocomplete="off" class="combo" name="select_multi_product2" id="select_multi_product2" style="width:300px;" _last="0"><option value="">-- Select Product 2 --</option>'; 
		while($row2 = db_fetch($res2)) {
			$selected = "";
			echo "<option value='".$row2['finish_pro_id']."' ".$selected.">".$row2['finish_product_name']."</option>";
		}
		echo '</select></td>';
	}
	
}
else if(isset($_REQUEST['range']))
{
	$range = $_REQUEST['range'];
	$sheet_type = $_REQUEST['sheet_type'];
	$sql = "SELECT * FROM 0_finish_product WHERE range_id=$range ORDER BY finish_product_name ASC";
	$res = db_query($sql, "Not retrieved");

	if($sheet_type == '1')
	{
		echo '<td class="label">Choose Product : </td>';
		echo '<td><select autocomplete="off" class="combo" name="select_single_product" id="select_single_product" style="width:300px;">';
		while($row = db_fetch($res)) {
			$selected = "";
			echo "<option value='".$row['finish_pro_id']."' ".$selected.">".$row['finish_product_name']."</option>";
		}
		echo '</select></td>';
	}else if($sheet_type == '2')
	{
		$sql2 = "SELECT * FROM 0_finish_product WHERE range_id=$range ORDER BY finish_product_name ASC";
		$res2 = db_query($sql2, "Not retrieved");
		echo '<td class="label">Choose Product : </td>';
		echo '<td><select autocomplete="off" class="combo" name="select_multi_product1" id="select_multi_product1" style="width:300px;" _last="0"><option value="">-- Select Product 1 --</option>'; 
		while($row = db_fetch($res)) {
			$selected = "";
			echo "<option value='".$row['finish_pro_id']."' ".$selected.">".$row['finish_product_name']."</option>";
		}
		echo '</select></td>';
		echo '<td><select autocomplete="off" class="combo" name="select_multi_product2" id="select_multi_product2" style="width:300px;" _last="0"><option value="">-- Select Product 2 --</option>'; 
		while($row2 = db_fetch($res2)) {
			$selected = "";
			echo "<option value='".$row2['finish_pro_id']."' ".$selected.">".$row2['finish_product_name']."</option>";
		}
		echo '</select></td>';
	}
}

?>