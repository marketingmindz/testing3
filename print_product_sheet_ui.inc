<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function product_sheet_types()
{
	echo '<select autocomplete="off" class="combo" name="product_sheet_type" id="product_sheet_type" style="width:300px;">'; 
		echo "<option value='-1'>--Select Sheet Type--</option>";
		echo "<option value='1'>Single Product Sheet</option>";
		echo "<option value='2'>Double Product Sheet</option>";
	echo '</select>';
}

function finish_product_single_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_arr)
{
	echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 
	
		echo "<option value=''>".$spec_opt."</option>";

		foreach ($product_arr as $product) 
		{	
			$selected = "";
			echo "<option value='".$product['finish_pro_id']."' ".$selected.">".$product['finish_product_name']."</option>";
		}
	echo '</select>';
}

// Reporting module - multiple select list
function finish_product_multiple_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_arr)
{
	echo '<div class="multiselect">';
    	echo '<div class="selectBox" onclick="showCheckboxes()">';

			echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 
			
				echo "<option value=''>".$spec_opt."</option>";

				foreach ($product_arr as $product) 
				{	
					$selected = "";
					echo "<option value='".$product['finish_pro_id']."' ".$selected.">".$product['finish_product_name']."</option>";
				}
			echo '</select>';

			//echo '<div id="sec_module_alert" style="color:red;">You can select maximum 2 products.</div>';

			echo '<div class="overSelect"></div>';
		echo '</div>';
    
    	echo '<div class="checkboxes" style="width: 35%;" >';
			
			foreach ($product_arr as $prod)
			{
				echo '<label>
							<input type="hidden" id="'.$prod['finish_pro_id'].'" value="'.$prod['finish_pro_id'].'" >
							<input type="checkbox" id="'.$prod['finish_pro_id'].'"  class="check" name="'.$name.'" value="'.$prod['finish_pro_id'].'" />'.$prod['finish_product_name'].
					'</label>';
			}	
    	echo '</div>';

    echo '</div>';
}
/****** shubham code start for filter type *******/
function product_filter_types()
{
	echo '<select autocomplete="off" class="combo" name="product_filter_type" id="product_filter_type" style="width:300px;">'; 
		echo "<option value='1'>Filter By Category</option>";
		echo "<option value='2'>Filter By Range</option>";
		echo "<option value='3'>Continue Without Filter</option>";
	echo '</select>';
} 

function finish_category_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_cat_arr)
{
	echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 

		foreach ($product_cat_arr as $product_cat) 
		{	
			$selected = "";
			echo "<option value='".$product_cat['category_id']."' ".$selected.">".$product_cat['cat_name']."</option>";
		}
	echo '</select>';
}
function finish_range_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_range_arr)
{
	echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 
	
		echo "<option value='-1' >Non Collection</option>";

		foreach ($product_range_arr as $product_range) 
		{	
			$selected = "";
			echo "<option value='".$product_range['id']."' ".$selected.">".$product_range['range_name']."</option>";
		}
	echo '</select>';
}

/****** shubham code end for filter type *******/

function showProductSelectionForm($primary_modules)
{
	$prodArray = array();
	$prodNameArray = array();

	$sql = "SELECT * FROM 0_finish_product ORDER BY finish_product_name ASC";
	$res = db_query($sql, "Not retrieved");
	while($row = db_fetch($res)) {
		array_push($prodArray, $row);
		array_push($prodNameArray, $row['finish_product_name']);
	}
	/****** shubham code start for get data for filter ****/
	$prod_cateArray = array();

	$cat_sql = "SELECT * FROM 0_item_category ORDER BY cat_name ASC";
	$cat_res = db_query($cat_sql, "Not retrieved");
	while($cat_row = db_fetch($cat_res)) {
		array_push($prod_cateArray, $cat_row);
	}

	$prod_rangeArray = array();

	$range_sql = "SELECT * FROM 0_item_range ORDER BY range_name ASC";
	$range_res = db_query($range_sql, "Not retrieved");
	while($range_row = db_fetch($range_res)) {
		array_push($prod_rangeArray, $range_row);
	}
	/****** shubham code end for get data for filter ****/
	div_start("report_details_div");
	start_table(TABLESTYLE_NOBORDER, "style=width:50%;");
	
	start_row();
		echo '<td class="label">Product Sheet Type : </td>';
		echo '<td>';
			product_sheet_types();
		echo '</td>';
	end_row();

	/********* shubham code start for filter *************/
	start_row("id=select_filter_row");
		echo '<td class="label">Choose Filter : </td>';
		echo '<td>';
			product_filter_types();
		echo '</td>';
	end_row();

	start_row("id=select_category_row");
		echo '<td class="label">Choose Category : </td>';
		echo '<td>';
			finish_category_list("select_category", @$_POST['select_single_product'], "-- Select Category --", true, $prod_cateArray);
		echo '</td>';
	end_row();

	start_row("id=select_range_row");
		echo '<td class="label">Choose Range : </td>';
		echo '<td>';
			finish_range_list("select_range", @$_POST['select_single_product'], "-- Select Range --", true, $prod_rangeArray);
		echo '</td>';
	end_row();

	/********* shubham code end for filter *************/

	start_row("id=select_single_product_row");
		echo '<td class="label">Choose Product : </td>';
		echo '<td>';
			finish_product_single_list("select_single_product", @$_POST['select_single_product'], "-- Select Product --", true, $prodArray);
		echo '</td>';
	end_row();

	start_row("id=select_multi_product_row");
		echo '<td class="label">Choose Product : </td>';
		echo '<td>';
			finish_product_single_list("select_multi_product1", @$_POST['select_multi_product1'], "-- Select Product 1 --", true, $prodArray);
		echo '</td>';
		echo '<td>';
			finish_product_single_list("select_multi_product2", @$_POST['select_multi_product2'], "-- Select Product 2 --", true, $prodArray);
		echo '</td>';
	end_row();
	/***** shubham code start for filter product row *****/
	start_row("id=select_single_product_filter_row");
		
	end_row();
	start_row("id=select_single_product_range_filter_row");
		
	end_row();
	/***** shubham code end for filter product row *****/

	start_row();
		submit_cells('print_sheet', _("Print"), "colspan=2", _('Print Product Sheet'), true);
		//echo "<input type='submit' name='print_sheet' />";
	end_row();

	end_table();

	div_end();
}

?>