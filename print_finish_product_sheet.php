<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root="..";
$page_security = 'SA_PRINT_FINISH_PRODUCT';

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/reporting/includes/db/report_db.inc");
include_once($path_to_root . "/reporting/includes/ui/print_product_sheet_ui.inc");
include_once($path_to_root . "/reporting/print_report.php");
include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

//-----------------------------------------------------------------------------------
?>

<style type="text/css">
	.multiselect { width: 200px; } 
	.selectBox { position: relative; } 
	.selectBox select { width: 100%; font-weight: bold; } 
	.overSelect { position: absolute; left: 0; right:0; top: 0; bottom: 0; } 
	.checkboxes { display: none; width: 200px; border: 1px #dadada solid; position: absolute; z-index: 999999999; background-color: #fff; max-height:300px; overflow:scroll; overflow-x:hidden;overflow-wrap: break-word; font-size: 14px;}
	.checkboxes label { display: block; border-bottom: 1px solid #ccc;  height: 21px;}
	.checkboxes label:hover { background-color: #1e90ff;}
	.optionGroup{ background-color: #ccc; white-space: nowrap;}
</style>
<?php
//-----------------------------------------------------------------------------------

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

add_js_file('reports.js');

global $Ajax;
page(_($help_context = "Print Finish Product Sheet"), false, false, "", $js);

//-----------------------------------------------------------------------------------

if(isset($_POST['print_sheet']))
{
	//------------------------------- PRINT SINGLE PRODUCT SHEET ----------------------------------------

	if($_POST['product_sheet_type'] == 1 && $_POST['select_single_product'] != '') {
		
		// get html
		$html = get_single_product_sheet_html($_POST['select_single_product']);

	    $headers = Array();
	    $aligns = Array();

	    $title = "Single Product Sheet";
	    $orientation = 'P';

	    $rep = new FrontReport($title, $title, user_pagesize(), 9, $orientation);

	    $rep->Font();
	    $rep->NewPage();

	    $rep->writeHTMLCell(500, 0, 40, 110, $html, '', '', '', true, '');

	    $rep->End();

	//------------------------------- PRINT MULTIPLE PRODUCT SHEET ----------------------------------------
		
	} else if($_POST['product_sheet_type'] == 2 && $_POST['select_multi_product1'] != '' && $_POST['select_multi_product2'] != '') {

		$html = ' <table width="100%" border="1" style="font-size:12px;" cellpadding="4">
				    <tr>
				    	<td width="250"> IMAGE </td>
				    	<td> COMMENTS </td>
				    	<td> DATE : </td>
				    </tr>';

		// get product 1 row
		$html .= get_multiple_product_sheet_html_row($_POST['select_multi_product1']);

		// get product 2 row
		$html .= get_multiple_product_sheet_html_row($_POST['select_multi_product2']);

		$html .= '</table>';

		$headers = Array();
	    $aligns = Array();

	    $title = "Double Product Sheet";
	    $orientation = 'L';

	    $rep = new FrontReport($title, $title, user_pagesize(), 9, $orientation);

	    $rep->Font();
	    $rep->NewPage();

	    $rep->writeHTMLCell(782, 0, 40, 95, $html, '', '', '', true, '');

	    $rep->End();
	
	} else if($_POST['product_sheet_type'] == ''){
		display_error('Select a Product Sheet Type.');
	} else if($_POST['product_sheet_type'] == 1 && $_POST['select_single_product'] == ''){
		display_error('Select a Product.');
	} else if($_POST['product_sheet_type'] == 2 && $_POST['select_multi_product1'] == ''){
		display_error('Select First Product.');
	} else if($_POST['product_sheet_type'] == 2 && $_POST['select_multi_product2'] == ''){
		display_error('Select Second Product.');
	}
}

//--------------------------------- GET HTML ROW FOR SINGLE PRODUCT SHEET ----------------------------

function get_single_product_sheet_html($product_id)
{
    global $path_to_root, $systypes_array, $site_url;

    include_once($path_to_root . "/reporting/includes/pdf_report.inc");

    $sql ='SELECT d.finish_comp_code,
    			d.finish_product_name, 
    			CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeName, 
    			c.cat_name, 
    			d.asb_weight, 
    			d.asb_height, 
    			d.asb_density,
    			d.product_image, 
    			d.description 
    		FROM '.TB_PREF.'finish_product d 
    		LEFT JOIN '.TB_PREF.'item_range t on d.range_id = t.id 
    		LEFT JOIN '.TB_PREF.'item_category c on d.category_id = c.category_id 
    		WHERE finish_pro_id="'.$product_id.'" ';
	
	$res = db_query($sql, "Not retrieved");
	$row = db_fetch($res);

	$rangeName = $row['rangeName'];
	$finishCode = $row['finish_comp_code'];
	$productName = $row['finish_product_name'];
	
	$productSize = '';
	if($row['asb_weight'] != '') { $productSize .= $row['asb_weight']; }
	if($row['asb_height'] != '') { $productSize .= " X ".$row['asb_height']; }
	if($row['asb_density'] != '') { $productSize .= " X ".$row['asb_density']; }

	$productImage = $row['product_image'];

	if($productImage == '')
	{
		$productImagePath = $site_url."/company/0/finishProductImage/no-image.jpg";
	} else {
		$productImagePath = $site_url."/company/0/finishProductImage/".$productImage;
	}

    $html = '
			<table border="0" style="font-size:11px;">
			    <tr>
			        <td width="210px" >
			        	<img src="'.$productImagePath.'" width="200px" height="150px"/>
			        </td>
			        
			        <td width="300px">
		        		<table border="0" cellpadding="6">
						    <tr>
						        <td  width="100px"> Product Name :</td>
						        <td  width="200px">'.$productName.'</td>
						    </tr>
						    <tr>
						        <td  width="100px"> Product Code :</td>
						        <td  width="200px">'.$finishCode.'</td>
						    </tr>
						    <tr>
						        <td  width="100px"> Range :</td>
						        <td  width="200px">'.$rangeName.'</td>
						    </tr>
						    <tr>
						        <td  width="100px"> Size :</td>
						        <td  width="200px">'.$productSize.'</td>
						    </tr>
						</table>
			        </td>
			    </tr>
			</table>';

	return $html;
}

//--------------------------------- GET HTML ROW FOR MULTIPLE PRODUCT SHEET ----------------------------

function get_multiple_product_sheet_html_row($product_id)
{
    global $path_to_root, $systypes_array, $site_url;

    include_once($path_to_root . "/reporting/includes/pdf_report.inc");

    $sql ='SELECT d.finish_comp_code,
    			d.finish_product_name, 
    			CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeName, 
    			c.cat_name, 
    			d.asb_weight, 
    			d.asb_height, 
    			d.asb_density,
    			d.product_image, 
    			d.description 
    		FROM '.TB_PREF.'finish_product d 
    		LEFT JOIN '.TB_PREF.'item_range t on d.range_id = t.id 
    		LEFT JOIN '.TB_PREF.'item_category c on d.category_id = c.category_id 
    		WHERE finish_pro_id="'.$product_id.'" ';
	
	$res = db_query($sql, "Not retrieved");
	$row = db_fetch($res);

	$rangeName = $row['rangeName'];
	$finishCode = $row['finish_comp_code'];
	$productName = $row['finish_product_name'];
	$pkgSize = $cbm = "";
	
	$productSize = '';
	if($row['asb_weight'] != '') { $productSize .= $row['asb_weight']; }
	if($row['asb_height'] != '') { $productSize .= " X ".$row['asb_height']; }
	if($row['asb_density'] != '') { $productSize .= " X ".$row['asb_density']; }

	$productImage = $row['product_image'];
	$productImage = trim($productImage,",");

	if($productImage == '')
	{
		$productImagePath = $site_url."/company/0/finishProductImage/no-image.jpg";
	} else {
		$productImagePath = $site_url."/company/0/finishProductImage/".$productImage;
	}

    $html .= ' <tr>
		    		<td width="250">
		    			<table border="0" style="font-size:10px;">
						    <tr>
						        <td>
						        	<img src="'.$productImagePath.'" width="240" height="110"/>
						        </td>
						    </tr>

						    <tr>    
						        <td>
					        		<table border="0" cellpadding="1">
									    <tr>
									        <td width="70px"> ITEM CODE :</td>
									        <td width="180px">'.$finishCode.'</td>
									    </tr>
									    <tr>
									        <td width="70px"> ITEM NAME :</td>
									        <td width="180px">'.$productName.'</td>
									    </tr>
									    <tr>
									        <td width="70px"> ITEM SIZE :</td>
									        <td width="180px">'.$productSize.'</td>
									    </tr>
									    <tr>
									        <td width="70px"> PKG SIZE :</td>
									        <td width="180px">'.$pkgSize.'</td>
									    </tr>
									    <tr>
									        <td width="70px"> CBM :</td>
									        <td width="180px">'.$cbm.'</td>
									    </tr>
									</table>
						        </td>
						    </tr>
						</table>
			    	</td>
		    		<td colspan="2"> </td>
		    </tr>';

	return $html;
}

//------------------------ DISPLAY PRODUCT SHEET FORM FORM -------------------------

start_form(true);

	showProductSelectionForm();

end_form();

//-----------------------------------------------------------------------------------

end_page();

?>

<script src="<?php echo $path_to_root . "/js/chosen/chosen.jquery.js"; ?>"></script>
<script src="<?php echo $path_to_root . "/js/add_new_report.js"; ?>"></script>
<script type="text/javascript">

    $(document).ready(function()
    {
	    $('#select_single_product_row').hide();
	    $('#select_multi_product_row').hide();
	    $('#select_category_row').hide();
	    $('#select_filter_row').hide();
	    $('#select_range_row').hide();
	    $('#select_single_product_filter_row').hide();
	    $('#select_single_product_range_filter_row').hide();

	    $('#product_sheet_type').change(function(){

	    	if($('#product_sheet_type').val() == '1') {
	            $('#select_single_product_row').hide();
	            $('#select_multi_product_row').hide();
	            $('#select_single_product_filter_row').hide();
	            $('#select_single_product_range_filter_row').hide();
	            $('#select_filter_row').show();
 
	        } else if($('#product_sheet_type').val() == '2') {

	            $('#select_single_product_row').hide();
	            $('#select_multi_product_row').hide();
	            $('#select_single_product_filter_row').hide();
	            $('#select_single_product_range_filter_row').hide();
	            $('#select_filter_row').show();

	        }else if($('#product_sheet_type').val() == '-1') {

	        	$('#select_single_product_row').hide();
	    		$('#select_multi_product_row').hide();
	    		$('#select_filter_row').hide();
	    		$('#select_category_row').hide();
	    		$('#select_single_product_filter_row').hide();
	    		$('#select_single_product_range_filter_row').hide();
	        }
	        	
	    });
	    $('#product_filter_type').change(function(){
	        
	        if($('#product_filter_type').val() == '1') {
	        	$('#select_multi_product_row').hide();
	        	$('#select_single_product_row').hide();
	        	$('#select_range_row').hide();
	        	$('#select_single_product_filter_row').hide();
	        	$('#select_single_product_range_filter_row').hide();
	            $('#select_category_row').show();
	        
	        } else if($('#product_filter_type').val() == '2') {
	        	$('#select_multi_product_row').hide();
	        	$('#select_single_product_row').hide();
	        	$('#select_single_product_filter_row').hide();
	        	$('#select_single_product_range_filter_row').hide();
	            $('#select_category_row').hide();
	            $('#select_range_row').show();

	        }else if($('#product_filter_type').val() == '3') {
	        	$('#select_multi_product_row').hide();
	        	$('#select_single_product_row').hide();
	        	$('#select_single_product_filter_row').hide();
	        	$('#select_single_product_range_filter_row').hide();
	        	$('#select_range_row').hide();
	    		$('#select_category_row').hide();

	        	if($('#product_sheet_type').val() == '1')
	        	{
	        		$('#select_single_product_row').show();
	        	}
	        	else if($('#product_sheet_type').val() == '2') {
		            $('#select_single_product_row').hide();
		            $('#select_multi_product_row').show();
		        }
	    		
	        }
	    });
	});

    $('#select_category').change(function(){
    	var sheet_type = $("#product_sheet_type").val();
    	var category = $(this).val();
    	$.ajax({
    		url: "product_filter_calling.php",
    		method: "POST",
    		data: { category : category , sheet_type: sheet_type},
    		success: function(data){
	    				$("#select_single_product_filter_row").empty();
		    			$("#select_single_product_filter_row").append(data);
		    			$("#select_single_product_filter_row").show();
    			}
    		});
	     });

    $('#select_range').change(function(){
    	var range = $(this).val();
    	var sheet_type = $("#product_sheet_type").val();
    	$.ajax({
    		url: "product_filter_calling.php",
    		method: "POST",
    		data: { range : range , sheet_type: sheet_type},
    		success: function(data){
		    			$("#select_single_product_range_filter_row").empty();
		    			$("#select_single_product_range_filter_row").append(data);
		    			$("#select_single_product_range_filter_row").show();
    			}
    		});
	     });
</script>

<link href="<?php echo $path_to_root . "/js/chosen/chosen.css"; ?>" rel='stylesheet' type='text/css'>